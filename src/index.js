import './styles.scss';
import { Snow } from './snow';

window.setTimeout(()=>{
    console.log("Start snowing");
    const snow = new Snow();
    snow.startStop();
    window.setTimeout(()=>snow.stop(),30000);
},1000);
